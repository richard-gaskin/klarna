const s3 = require('s3');
const path = require('path');

const pjson = require(path.join(__dirname, '../', 'package.json'));
const config = {
  key: "AKIAJI7ZJJXXH2QUSTYQ",
  secret: "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",
  bucket: "jdsports-client-resources",
};

const client = s3.createClient({
  s3Options: {
    accessKeyId: config.key,
    secretAccessKey: config.secret,
  },
});

const params = {
  localDir: path.join(__dirname, '../', 'dist/'),

  s3Params: {
    Bucket: config.bucket,
    Prefix: pjson.deployment['s3:location'],
  },
};

const uploader = client.uploadDir(params);
let previousTotal = 0;

uploader.on('error', err => console.info('unable to sync', err.stack));

uploader.on('progress', () => {
  const total = uploader.progressTotal;
  const current = uploader.progressAmount;
  const percentageProgress = `${((100 / total) * current).toFixed(0)}%`;

  if (current > 0 && percentageProgress !== previousTotal) {
    previousTotal = percentageProgress;
  }
});

uploader.on('end', () => console.info(`deployment to ${params.s3Params.Prefix} complete`));
