const fs = require('fs');
const path = require('path');
const program = require('commander');

function getFiles(dir, files_) {
  files_ = files_ || [];

  const files = fs.readdirSync(dir);

  for (const i in files) {
    const name = `${dir}/${files[i]}`;
    if (fs.statSync(name).isDirectory()) {
      getFiles(name, files_);
    } else {
      files_.push(name);
    }
  }

  return files_;
}

function writeVersion(platform = 'uat') {
  const all = getFiles('./dist');

  const files = all
    .filter((file) => {
      if (/(app|chunk-[a-zA-Z]+?).[a-z0-9]{8}.(js|css)$/.test(file)) {
        return file;
      }

      return false;
    })
    .map((file) => {
      if (file.includes('css')) {
        return { css: file.match(/[a-z0-9]{8}/)[0] };
      }

      if (file.includes('vendors')) {
        return { vendor: file.match(/[a-z0-9]{8}/)[0] };
      }

      if (file.includes('app')) {
        return { app: file.match(/[a-z0-9]{8}/)[0] };
      }

      return false;
    })
    .reduce((acc, x) => {
      for (const key in x) acc[key] = x[key];
      return acc;
    }, {});

  fs.writeFile(
    path.join(__dirname, '../', `build/version-${platform}.js`),
    `export default ${JSON.stringify(files)};\n`, (err) => {
      // throws an error, you could also catch it here
      if (err) throw err;

      // success case, the file was saved
      console.log('version updated for', platform);
    });
}

function uat() {
  writeVersion('uat');
}

function live() {
  writeVersion('live');
}

program
  .version('0.0.1')
  .description('Version deployment - deployment made easy')
  .option('-d, --development', 'Deploy to UAT', uat)
  .option('-p, --production', 'Deploy to Production', live)
  .parse(process.argv);
