import uat from './version-uat';
import live from './version-live';

const version = { uat, live };
const v = (/localhost|uatmesh/.test(window.location.host)) ? version.uat : version.live;
const tag = document.querySelector('script[data-root]');
const base = tag.getAttribute('data-root');

function loadScript(src, cb) {
  const script = document.createElement('script');
  script.src = src;
  document.head.appendChild(script);

  if (cb) cb();
}

// load js
// eslint-disable-next-line
loadScript(base + 'js/chunk-vendors.' + v.vendor + '.js', function () {
  // eslint-disable-next-line
  loadScript(base + 'js/app.' + v.app + '.js')
});

// load styling
const css = document.createElement('link');
css.rel = 'stylesheet';
// eslint-disable-next-line
css.href = base + 'css/app.' + v.css + '.css';

document.head.appendChild(css);
