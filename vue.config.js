module.exports = {
  baseUrl: (process.env.NODE_ENV === 'production')
    ? 'https://jdsports-client-resources.co.uk/jdsports-client-resources/pages/klarna/page'
    : './',
};

